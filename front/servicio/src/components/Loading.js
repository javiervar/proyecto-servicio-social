import { React } from 'react';



function Loading(props) {

    const setImage = () => {
        console.log(props.image)
        switch (props.image) {
            case "science":
                return require("../assets/loadings/loading4.gif")
            case "cat":
                return require("../assets/loadings/loading7.gif")
            case "cat2":
                return require("../assets/loadings/loading8.gif")
            default:
                return require("../assets/loadings/loading4.gif")
        }
    }
    return <img src={setImage()} style={{ width: props.size, display: "block", margin: "0 auto" }} />
}


Loading.defaultProps = {
    size: "50%",
    image: "science"
}

export default Loading;