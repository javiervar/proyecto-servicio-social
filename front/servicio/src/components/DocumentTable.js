import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Table } from 'react-bootstrap';
import IconButton from '@material-ui/core/IconButton';
import { useHistory } from "react-router-dom";
import CONSTANT from '../store/constant';
import Tooltip from '@material-ui/core/Tooltip';


import { DeleteOutlineOutlined, InfoOutlined, PictureAsPdf, EditOutlined, GetAppOutlined } from '@material-ui/icons';


const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(1),
    },
    extendedIcon: {
        marginRight: theme.spacing(1),
    },
}));

export default function DocumentTable(props) {
    const classes = useStyles();
    const history = useHistory();


    const handleClickDetails = (id, type) => {
        history.push("/file", { Id: id, DocumentType: type });
    };
    const handleClickEdit = (id, type) => {
        history.push("/edit", { Id: id, DocumentType: type });
    };


    const downloadFile = (Id) => {
        var download_link = CONSTANT.API_PATH + "/download/" + Id;
        window.location.replace(download_link);
    }

    const handleDelete = (Id) => {
        props.handleDelete(Id)
    }

    const icon = (format) => {

        switch (format) {
            case 1:
                return <img src={require("../assets/img/pdf.png")} style={{ width: 25, marginRight: 15 }} />
            case 2:
                return <img src={require("../assets/img/image.png")} style={{ width: 25, marginRight: 15 }} />
            case 3:
                return <img src={require("../assets/img/doc.png")} style={{ width: 25, marginRight: 15 }} />
            case 4:
            case 5:
                return <img src={require("../assets/img/excel.png")} style={{ width: 25, marginRight: 15 }} />
            default:
                return <img src={require("../assets/img/default.png")} style={{ width: 25, marginRight: 15 }} />
        }
    }

    const getDocumentsRow = () => {
        const rows = props.documents.map((row, index) => {
            return (
                <tr key={index} >

                    <td style={{ verticalAlign: "middle" }}>
                        {icon(row.format_fk)}
                        {row.document_name}
                    </td>

                    <td style={{ verticalAlign: "middle" }}>{row.document_type}</td>
                    <td style={{ verticalAlign: "middle" }}>
                        {
                            new Date(row.upload_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                        }</td>
                    <td style={{ verticalAlign: "middle" }}>
                        <div>
                            <Tooltip title="Detalles">
                                <IconButton
                                    aria-label="Info"
                                    size="small"
                                    className={classes.margin}
                                    onClick={() => handleClickDetails(row.document_id, row.document_type_fk)}>
                                    <InfoOutlined
                                        fontSize="small"
                                    />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Modificar">
                                <IconButton
                                    className={classes.margin}
                                    aria-label="Edit"
                                    size="small"
                                    onClick={() => handleClickEdit(row.document_id, row.document_type_fk)}>
                                    <EditOutlined
                                        fontSize="small"
                                    />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Descargar">
                                <IconButton
                                    className={classes.margin}
                                    aria-label="Delete"
                                    size="small"
                                    onClick={() => downloadFile(row.document_id)}>
                                    <GetAppOutlined
                                        fontSize="small"
                                    />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Eliminar">
                                <IconButton
                                    className={classes.margin}
                                    aria-label="Delete"
                                    size="small"
                                    onClick={() => handleDelete(row.document_id)}>
                                    <DeleteOutlineOutlined
                                        fontSize="small"
                                    />
                                </IconButton>
                            </Tooltip>
                        </div>
                    </td>
                </tr>
            );
        })
        return rows;
    }



    return (

        <Table size="sm">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Tipo</th>
                    <th>Fecha subida</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                {getDocumentsRow()}
            </tbody>
        </Table>
    );

}

