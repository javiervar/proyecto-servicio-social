import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';


export default function SimpleSelect(props) {


    const handleChange = event => {
        props.onChange(event.target.value)
    };
    const renderOption=()=>{
        const{options}=props;
        return options.map((item,index)=>{
            return (
                <MenuItem key={index} value={item.value}>{item.label}</MenuItem>
            )
        })
    }

    return (

        <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            name={props.name}
            defaultValue={-1}
            onChange={handleChange}
        >
            <MenuItem value={-1}>{props.default_option}</MenuItem>
            {renderOption()}
        </Select>

    );
}