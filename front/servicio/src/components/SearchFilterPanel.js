import React from 'react';
import './searchBar.css';
import Select from 'react-select';
import axios from 'axios';
import { Form, Row, Col, Button, Navbar, Container, Card, Collapse } from 'react-bootstrap';



class SearchFilterPanel extends React.Component {
    state = { selectedOption: null,options:[] };

    componentDidMount() {
        this.getDocumentTypesItem();
    }

    getDocumentTypesItem = () => {

        axios.get("http://localhost:8000/document_type_item")
            .then((response) => {
                console.log(response);
                this.setState({ options: response.data })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {

        return (
            <Card className={"filterPanel"}>
                <Card.Body>
                    <Form.Group as={Row} controlId="formPlaintextEmail">
                        <Form.Label column sm="2">
                            Tipo
                     </Form.Label>
                        <Col sm="10">
                            <Select
                                name="colors"
                                options={this.state.options}
                                className="basic-multi-select"
                                classNamePrefix="select"
                            />
                        </Col>
                    </Form.Group>
                    <Button variant="primary" style={{ float: "right", marginTop: 20 }}>Buscar</Button>
                </Card.Body>
            </Card>
        )
    }
}

export default SearchFilterPanel;