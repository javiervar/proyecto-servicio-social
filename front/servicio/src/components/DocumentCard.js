import React from 'react';
import { Card, Button, Col, Row } from 'react-bootstrap';
class DocumentCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { spans: 0 };
    }

    componentDidMount() {
        //this.imageRef.current.addEventListener('load',this.setSpans);
    }
    fileIcon = (format) => {
        if (format == 1) {
            return <i className="fas fa-file-pdf "></i>;
        } else if (format == 2) {
            return <i className="fas fa-file-image primary"></i>
        }

        return null;
    }
    render() {

        return (
            <Row>
                <Col xs={1} style={{fontSize:30}}>
                    {this.fileIcon(this.props.info.format_fk)}
                </Col>
                <Col xs={3}>
                    <p>{this.props.info.document_name}</p>
                    <span>{this.props.info.document_type}</span>
                </Col>
                <Col>

                </Col>

            </Row >
        )
    }
}

export default DocumentCard;