import './ImageList.css'
import React from 'react';
import DocumentCard from './DocumentCard';
import { Row } from 'react-bootstrap';


const DocumentList=(props)=>{
    const documents=props.documents.map((document)=>{
        return <DocumentCard key={document.id} info={document}  />
    })

   // return <div className="image-list">{documents}</div>
   return <div>{documents}</div>
   
}

export default DocumentList;