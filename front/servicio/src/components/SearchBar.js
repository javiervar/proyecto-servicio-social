import React from 'react';
import { Form, Row, Col, Container, Card, Collapse } from 'react-bootstrap';
import Button from '@material-ui/core/Button';

import './searchBar.css';
import axios from 'axios';
import CONSTANT from '../store/constant';
import Select from "../components/Select";
import IconButton from '@material-ui/core/IconButton';

import BookForm from "../pages/home/Forms/Book";
import CertificateForm from "../pages/home/Forms/Certificate";
import CovenantForm from "../pages/home/Forms/Covenant";
import ResidenceForm from "../pages/home//Forms/Residence";
import AcademicStayForm from '../pages/home/Forms/AcademicStay';


import InlineDatePickerDemo from "../components/DatePickerInput";
import { ExpandMore, ExpandLess, Search } from '@material-ui/icons';
import EntailmentForm from '../pages/home/Forms/Entailement';
import CongressForm from '../pages/home/Forms/Congress';
import SymposiumForm from '../pages/home/Forms/Symposium';
import Colloquium from '../pages/home/Forms/Colloquium';

import JCR_1 from '../pages/home/Forms/JCR_1';
import JCR_2 from '../pages/home/Forms/JCR_2';
import International_paper_1 from '../pages/home/Forms/International_paper_1';
import International_paper_2 from '../pages/home/Forms/International_paper_2';
import National_paper_1 from '../pages/home/Forms/National_paper_1';
import National_paper_2 from '../pages/home/Forms/National_paper_2';
import Other from '../pages/home/Forms/Other';



const uploadDateOpts = [
    { label: "Hoy", value: 1 },
    { label: "Ayer", value: 2 },
    { label: "Últimos 7 días", value: 3 },
    { label: "Últimos 30 días", value: 4 },
    { label: "Personalizado", value: 5 },
]


class SearchBar extends React.Component {
    state = {
        term: '',
        filterPanel: false,
        selectedOption: null,
        libro: false,
        certificado: false,
        convenio: false,
        vinculacion: false,
        estancia: false,
        personalizado: false,
        options: [],
        documentType: -1
    };

    componentDidMount() {
        this.getDocumentTypesItem();
    }

    getDocumentTypesItem = () => {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;
        axios.get("http://localhost:8000/api/document_type_item")
            .then((response) => {
                console.log(response);
                this.setState({ options: response.data })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    onFormSubmit = (event) => {
        event.preventDefault();

        this.props.onSubmit(this.state.term);
    }
    toggleFilterPanel = () => {
        this.setState({
            filterPanel: !this.state.filterPanel
        })
    }
    handleInputChange = (val) => {
        console.log(val)
        for (var opt of val) {
            switch (val.id) {
                case 1://Libro
                    this.setState()
                    break;
            }
        }
    };

    typeChangeHandler = (val) => {
        console.log("select value", val)
        this.setState({
            documentType: val
        })
    }

    dateChangeHandler = (val) => {
        console.log("select value", val)
        if (val == 5) {
            this.setState({
                personalizado: !this.state.personalizado
            })
        }
    }

    onFirstFormSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        let document = Object.fromEntries(data);
        document.page = 0;
        this.toggleFilterPanel();
        this.props.onFilterSubmit(document)
    }

    changeForm = () => {

        switch (this.state.documentType) {
            case 1:
                return <BookForm />;
            case 2:
                return <CertificateForm />;
            case 3:
                return <CovenantForm />;
            case 4:
                return <EntailmentForm />;
            case 5:
                return <AcademicStayForm />;
            case 6:
                return <ResidenceForm />;
            case 7:
                return <CongressForm />;
            case 8:
                return <SymposiumForm />;
            case 9:
                return <Colloquium />;
            case 10:
                return <JCR_1 />;
            case 11:
                return <JCR_2 />;
            case 12:
                return <International_paper_1 />;
            case 13:
                return <International_paper_2 />;
            case 14:
                return <National_paper_1 />;
            case 15:
                return <National_paper_2 />;
            case 16:
                return <Other />;
            case 17:


            default:
                return null;


        }

    }

    render() {
        return (
            <Container>
                <Form onSubmit={this.onFormSubmit} className={"search-panel"}>
                    <Row className={"searchRow"}>
                        <Col md={1} className={"searchCol"}>

                            <IconButton onClick={this.toggleFilterPanel} >
                                <Search fontSize="inherit" />
                            </IconButton>
                        </Col>
                        <Col md={10} className={"searchCol"}>
                            <Form.Control
                                size="lg"
                                type="text"
                                value={this.state.term}
                                className={"searchInput"}
                                placeholder={"Buscar"}
                                onChange={(e) => { this.setState({ term: e.target.value }) }}
                            />
                        </Col>
                        <Col md={1} className={"searchCol"}>

                            <IconButton onClick={this.toggleFilterPanel} >
                                {!this.state.filterPanel ? (
                                    <ExpandMore fontSize="inherit" />
                                ) : (
                                        <ExpandLess fontSize="inherit" />
                                    )}
                            </IconButton>
                        </Col>
                    </Row>
                </Form>
                <Collapse in={this.state.filterPanel} style={{ width: 600, position: "absolute",right:0,left:0,margin:"auto" }}>
                    <Card className={"filterPanel"}>


                        <Card.Body>
                            <Form onSubmit={this.onFirstFormSubmit}>

                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Fecha de subida
                                         </Form.Label>
                                    <Col sm="8">
                                        <Select default_option="Cualquier momento" name="upload_date" options={uploadDateOpts} onChange={this.dateChangeHandler} />
                                    </Col>
                                </Form.Group>



                                {this.state.personalizado ? (
                                    <div style={{ padding: 20 }} >
                                        <InlineDatePickerDemo />
                                    </div>
                                ) : null}



                                <Form.Group as={Row} controlId="formPlaintextEmail">
                                    <Form.Label column sm="4">
                                        Tipo
                                         </Form.Label>
                                    <Col sm="8">
                                        <Select default_option="Todos los documentos" name="document_type" options={this.state.options} onChange={this.typeChangeHandler} />
                                    </Col>
                                </Form.Group>

                                {this.state.documentType > 0 ? (
                                    <div style={{ height: 2, background: "#ddd", margin: 10, width: "100%" }}></div>
                                ) : null}

                                {this.changeForm()}


                                <Button type="submit" variant="contained" style={{ float: "right", marginTop: 20, background: "#2196f3", color: "#fff" }}>
                                    Buscar
                                    </Button>
                            </Form>

                        </Card.Body>
                    </Card>
                </Collapse>


            </Container>
        );
    }
}

export default SearchBar;