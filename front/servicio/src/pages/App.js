import React from 'react';
import NavBarComponent from './navbar/NavBarComponent';
import { Container, Row, Col } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Home from './home/Home';
import File from './file/File';
import Edit from './file/Edit';

import Auth from './auth/Auth';
//import Detail from '../pages/home/Detail';

const fakeAuth = {
    isAuthenticated: false,
    authenticate(cb) {
        this.isAuthenticated = true
        setTimeout(cb, 100)
    },
    signout(cb) {
        this.isAuthenticated = false
        setTimeout(cb, 100)
    }
}


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={(props) => (
        fakeAuth.isAuthenticated === true
            ? <Component {...props} />
            : <Redirect to='/' />
    )} />
)

class App extends React.Component {


    render() {
        return (
            <Router>

                <Switch>
                    <Route exact path='/' component={Auth} />
                    <Route path='/home' component={Home} />
                    <Route path='/file' component={File} />
                    <Route path='/edit' component={Edit} />
                </Switch>

            </Router>

        );
    }
};

export default App;


