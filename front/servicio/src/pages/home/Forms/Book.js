import React from 'react';
import { Form, Col } from 'react-bootstrap';

function BookForm(props) {
    return (
        <div>
            
            <Form.Group>
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" name="title" required={props.required} defaultValue={props.info.title}/>
            </Form.Group>

            <Form.Group  >
                <Form.Label>Autor</Form.Label>
                <Form.Control type="text" name="author" required={props.required} defaultValue={props.info.author}/>
            </Form.Group>

            <Form.Row>
                <Form.Group as={Col} >
                    <Form.Label>Año</Form.Label>
                    <Form.Control type="number" name="year" required={props.required} defaultValue={props.info.year}/>
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Edición</Form.Label>
                    <Form.Control type="text" name="edition" required={props.required} defaultValue={props.info.edition}/>
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Páginas</Form.Label>
                    <Form.Control type="number" name="pages" required={props.required} defaultValue={props.info.pages}/>
                </Form.Group>

            </Form.Row>

            <Form.Group  >
                <Form.Label>Editorial</Form.Label>
                <Form.Control type="text" name="publisher" required={props.required} defaultValue={props.info.publisher}/>
            </Form.Group>

            <Form.Group  >
                <Form.Label>Lugar de publicación</Form.Label>
                <Form.Control type="text" name="publication_place" required={props.required} defaultValue={props.info.publication_place}/>
            </Form.Group>


        </div>
    )
}

BookForm.defaultProps = {
    required:false,
    info:{}   
}

export default BookForm;