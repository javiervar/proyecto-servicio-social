import React from 'react';
import { Form, Col } from 'react-bootstrap';


function SymposiumForm(props) {
    return (
        <div>
            
            <Form.Group  >
                <Form.Label>Tema</Form.Label>
                <Form.Control type="text" name="tema" required={props.required} defaultValue={props.info.tema}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Expositores</Form.Label>
                <Form.Control type="text" name="exhibitors" required={props.required} defaultValue={props.info.exhibitors}/>
            </Form.Group>
            
            
            <Form.Row>
                <Form.Group as={Col} >
                    <Form.Label>Fecha inicio</Form.Label>
                    <Form.Control type="date" name="start_date" required={props.required} defaultValue={new Date(props.info.start_date).toISOString().substr(0,10)}/>
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Fecha fin</Form.Label>
                    <Form.Control type="date" name="finish_date" required={props.required} defaultValue={new Date(props.info.finish_date).toISOString().substr(0,10)}/>
                </Form.Group>
            </Form.Row>



        </div>
    )
}

SymposiumForm.defaultProps = {
    required:false,
    info:{}    
}

export default SymposiumForm;