import React from 'react';
import { Form, Col } from 'react-bootstrap';


function CertificateForm(props) {
    return (
        <div>

            <Form.Group  >
                <Form.Label>Nombre</Form.Label>
                <Form.Control type="text" name="owner" required={props.required} defaultValue={props.info.owner} />
            </Form.Group>


            <Form.Row>
                <Form.Group as={Col} >
                    <Form.Label>Fecha inicio</Form.Label>
                    <Form.Control type="date" name="start_date" required={props.required} defaultValue={new Date(props.info.start_date).toISOString().substr(0, 10)} />
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Fecha fin</Form.Label>
                    <Form.Control type="date" name="finish_date" required={props.required} defaultValue={new Date(props.info.finish_date).toISOString().substr(0, 10)} />
                </Form.Group>
            </Form.Row>

            <Form.Group  >
                <Form.Label>Quien lo otorga</Form.Label>
                <Form.Control type="text" name="who_grants" required={props.required} defaultValue={props.info.who_grants} />
            </Form.Group>

            <Form.Group  >
                <Form.Label>Tiempo</Form.Label>
                <Form.Control type="text" name="time" required={props.required} defaultValue={props.info.time} />
            </Form.Group>

            <Form.Group  >
                <Form.Label>Nombre del certificado</Form.Label>
                <Form.Control type="text" name="certification_name" required={props.required} defaultValue={props.info.certification_name} />
            </Form.Group>

            <Form.Group  >
                <Form.Label>Periodo</Form.Label>
                <Form.Control type="text" name="period" required={props.required} defaultValue={props.info.period} />
            </Form.Group>

        </div>
    )
}

CertificateForm.defaultProps = {
    required: false,
    info: {}
}

export default CertificateForm;