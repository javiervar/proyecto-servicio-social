import React from 'react';
import { Form, Col } from 'react-bootstrap';


function CovenantForm(props) {
    return (
        <div>
            
            <Form.Group  >
                <Form.Label>Compañía</Form.Label>
                <Form.Control type="text" name="company_name" required={props.required} defaultValue={props.info.company_name}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Duración</Form.Label>
                <Form.Control type="text" name="duration" required={props.required} defaultValue={props.info.duration}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Nombre del proyecto</Form.Label>
                <Form.Control type="text" name="project_name" required={props.required} defaultValue={props.info.project_name}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Colaboradores</Form.Label>
                <Form.Control type="text" name="collaborators" required={props.required} defaultValue={props.info.collaborators}/>
            </Form.Group>

            
            <Form.Row>
                <Form.Group as={Col} >
                    <Form.Label>Fecha inicio</Form.Label>
                    <Form.Control type="date" name="start_date" required={props.required} defaultValue={new Date(props.info.start_date).toISOString().substr(0,10)}/>
                </Form.Group>
                <Form.Group as={Col} >
                    <Form.Label>Fecha fin</Form.Label>
                    <Form.Control type="date" name="finish_date" required={props.required} defaultValue={new Date(props.info.finish_date).toISOString().substr(0,10)}/>
                </Form.Group>
            </Form.Row>



        </div>
    )
}

CovenantForm.defaultProps = {
    required:false    
}

export default CovenantForm;