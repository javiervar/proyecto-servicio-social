import React from 'react';
import { Form, Col } from 'react-bootstrap';

function Other(props) {
    return (
        <div>

            <Form.Group  >
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" name="title" required={props.required} defaultValue={props.info.title}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Autor(es)</Form.Label>
                <Form.Control type="text" name="author" required={props.required} defaultValue={props.info.author}/>
            </Form.Group>
            <Form.Group  >
                <Form.Label>Descripción</Form.Label>
                <Form.Control type="text" name="description" required={props.required}  defaultValue={props.info.description}/>
            </Form.Group>
            

    

        </div>
    )
}

Other.defaultProps = {
    required: false,
    info:{}
}

export default Other;