import React from 'react';
import { Form, Col } from 'react-bootstrap';

function JCR_2(props) {
    return (
        <div>

            <Form.Group  >
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" name="title" required={props.required} defaultValue={props.info.title}/>
            </Form.Group>

            <Form.Group  >
                <Form.Label>Autor(es)</Form.Label>
                <Form.Control type="text" name="author" required={props.required} defaultValue={props.info.author}/>
            </Form.Group>
            <Form.Group as={Col} >
                <Form.Label>Año</Form.Label>
                <Form.Control type="number" name="year" required={props.required} defaultValue={props.info.year}/>
            </Form.Group>

            <Form.Group  >
                <Form.Label>Referencias</Form.Label>
                <Form.Control type="text" name="reference" required={props.required} defaultValue={props.info.reference}/>
            </Form.Group>

    

        </div>
    )
}

JCR_2.defaultProps = {
    required: false,
    info:{}
}

export default JCR_2;