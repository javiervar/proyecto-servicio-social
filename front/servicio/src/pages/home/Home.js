import React from 'react';
import SearchBar from '../../components/SearchBar';
import DocumentTable from '../../components/DocumentTable';
import NavBarComponent from '../navbar/NavBarComponent';
import axios from 'axios';
import { Container, Card, Col, Modal } from 'react-bootstrap';
import Button from '@material-ui/core/Button';
import CONSTANT from '../../store/constant';
import FormModal from './Components/FormModal'
import { Add, Warning } from '@material-ui/icons';
import ReactPaginate from 'react-paginate';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import './Components/table.css'


class Home extends React.Component {
    state = {
        documents: [],
        sidebarOpen: false,
        options: [],
        pages: 1,
        currentPage: 0,
        params: {},
        modalDelete: false,
        deleteId: 0,
        loading: true
    }

    onSearchSubmit = async (term) => {
        console.log(term)

        var params = {
            txt: term,
            page: 0,
        }

        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;
        axios.get(CONSTANT.API_PATH + "/api/search", { params })
            .then((response) => {
                console.log(response);
                if (params.page == 0) {
                    this.setState({ documents: response.data.data, pages: response.data.pages, currentPage: 0 })
                } else {
                    this.setState({ documents: response.data.data, pages: response.data.pages })
                }

            })
            .catch((error) => {
                console.log(error);
            })


    }
    onSearchSubmit_filter = async (params) => {
        console.log("filter submit:", params);
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;
        axios.get(CONSTANT.API_PATH + "/api/search", { params })
            .then((response) => {
                console.log(response);
                if (params.page == 0) {
                    this.setState({
                        documents: response.data.data,
                        pages: response.data.pages,
                        params: params,
                        currentPage: 0
                    });
                } else {
                    this.setState({
                        documents: response.data.data,
                        pages: response.data.pages,
                        params: params
                    })
                }
            })
            .catch((error) => {
                console.log(error);
            })


    }
    componentWillMount() {
        const userInfo = JSON.parse(localStorage.getItem("userInfo"));
        CONSTANT.TOKEN = userInfo.token;
        console.log(userInfo)
        this.getFiles();
    }

    getFiles = () => {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;
        var params = {
            page: 0
        }
        axios.get(CONSTANT.API_PATH + "/api/documents", { params })
            .then((response) => {
                console.log(response);
                this.setState({
                    documents: response.data.data,
                    pages: response.data.pages,
                    loading:false,
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }


    handlePageClick = data => {
        let selected = data.selected;
        console.log("sel" + selected)
        let { params } = this.state;
        params.page = selected;
        this.onSearchSubmit_filter(params);
    };

    onSetSidebarOpen = (open) => {
        this.setState({ sidebarOpen: open });
    }
    openModal = () => {
        this.setState({
            openModal: !this.state.openModal
        })
    }

    handleModalDelete = (id) => {
        console.log(id)
        this.setState({
            modalDelete: !this.state.modalDelete,
            deleteId: id
        })
    }

    handleDelete = () => {
        const { deleteId } = this.state;
        const data = {
            id: deleteId
        }

        axios.delete(CONSTANT.API_PATH + "/api/delete", { data })
            .then((response) => {
                console.log(response);
                this.setState({
                    loading:false,
                    modalDelete: false,
                }, () => {
                    this.getFiles();
                })
            })
            .catch((error) => {
                console.log(error);
            })


    }

    formSended = (val) => {
        this.openModal();
        alert(val);
        this.getFiles();

    }
    downloadFile = (Id) => {
        var download_link = CONSTANT.API_PATH + "/download/" + Id;
        window.location.replace(download_link);
    }

    
    render() {
        return (
            <div>
                <NavBarComponent />
                <SearchBar onSubmit={this.onSearchSubmit} onFilterSubmit={this.onSearchSubmit_filter} />
                <FormModal show={this.state.openModal} handleClose={this.openModal} done={this.formSended} />
                <Modal show={this.state.modalDelete} onHide={this.handleModalDelete}>
                    <Modal.Header style={{ background: "#d50000" }}>
                        <Warning style={{ color: "#fff" }} />
                    </Modal.Header>
                    <Modal.Body>
                        ¿Estás seguro de que deseas eliminar este documento?
                        </Modal.Body>
                    <Modal.Footer>
                        <Button variant="contained" size="small" onClick={this.handleModalDelete}>
                            Cancelar
                        </Button>
                        <Button variant="contained" size="small" style={{ background: "#d50000", color: "#fff" }} onClick={this.handleDelete}>
                            Eliminar
                        </Button>
                    </Modal.Footer>
                </Modal>
                <Container style={{ marginTop: 50 }}>
                    <Button variant="contained"  onClick={this.openModal} style={{ float: "right", marginBottom: 15, background: "rgb(0, 150, 136)", color: "#fff" }}>
                        Nuevo <Add style={{ marginLeft: 5}} />
                    </Button>


                    <Paper style={{ padding: 5 }}>
                        {this.state.loading ? (//4  o la 5 cambiando de color de fondo a #f5f5f5
                            <img src={require("../../assets/loadings/loading4.gif")} style={{ width: "50%", display: "block", margin: "0 auto" }} />

                        ) : (
                                <div>
                                    <DocumentTable documents={this.state.documents} handleDelete={this.handleModalDelete} />
                                    <ReactPaginate
                                        previousLabel={(<i className="fas fa-chevron-left"></i>)}
                                        nextLabel={(<i className='fas fa-chevron-right'></i>)}
                                        breakLabel={'...'}
                                        breakClassName={'break-me'}
                                        initialPage={0}
                                        forcePage={this.state.currentPage}
                                        pageCount={this.state.pages}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={this.handlePageClick}
                                        containerClassName={'pagination'}
                                        subContainerClassName={'pages pagination'}
                                        activeClassName={'active'}
                                    />

                                </div>

                            )}
                    </Paper>
                </Container>

            </div >

        );
    }
};

export default Home;


