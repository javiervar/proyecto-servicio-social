import React from 'react';
import { Modal, Form, Col, Button } from 'react-bootstrap';
import FileUploader from './FileUploader';
import axios from 'axios';
import BookForm from "../Forms/Book";
import CertificateForm from "../Forms/Certificate";
import CovenantForm from "../Forms/Covenant";
import ResidenceForm from "../Forms/Residence";
import AcademicStayForm from '../Forms/AcademicStay';

import EntailmentForm from '../Forms/Entailement';
import CongressForm from '../Forms/Congress';
import SymposiumForm from '../Forms/Symposium';
import Colloquium from '../Forms/Colloquium';

import JCR_1 from '../Forms/JCR_1';
import JCR_2 from '../Forms/JCR_2';
import International_paper_1 from '../Forms/International_paper_1';
import International_paper_2 from '../Forms/International_paper_2';
import National_paper_1 from '../Forms/National_paper_1';
import National_paper_2 from '../Forms/National_paper_2';
import Other from '../Forms/Other';


import CONSTANT from '../../../store/constant';
import CircularProgress from '@material-ui/core/CircularProgress';

class FormModal extends React.Component {
    state = { options: [], documentType: 1, selectedFile: null, loading: false }

    componentWillMount() {
        this.getDocumentTypesItem();
    }
    getDocumentTypesItem = () => {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;
        axios.get(CONSTANT.API_PATH + "/api/document_type_item")
            .then((response) => {
                this.setState({ options: response.data })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    loadOptions = () => {
        const options = this.state.options.map((opc, index) => {
            return <option key={index} value={opc.value}>{opc.label}</option>;
        })
        return options;
    }
    onFormSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        const document = Object.fromEntries(data);
        this.setState({
            loading: true
        })

        //SAVE FILE
        const dataFile = new FormData()
        dataFile.append('document', document.file);

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/upload-document", dataFile)
            .then((response) => {

                const resp = response.data.data;
                this.saveDocument(resp, document)
            })
            .catch((error) => {
                console.log(error);
            })
    }

    saveDocument = (data, document) => {
        data.type = this.state.documentType;
        var format = 2;//image
        if (data.mimetype == "application/pdf") {
            format = 1
        } else if (data.mimetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
            format = 3;
        } else if (data.mimetype == "application/vnd.ms-excel") {
            format = 4;
        } else if (data.mimetype == "text/csv") {
            format = 5;
        }
        data.format_fk = format;

        console.log("-->", data)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/save-file", data)
            .then((response) => {
                const id = response.data.insertId;
                document.document_fk = id;
                document.document_type = this.state.documentType;
                this.saveDocument2(document);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    saveDocument2 = (document) => {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/save-document", document)
            .then((response) => {
                console.log(response);

                this.setState({
                    loading: false
                }, () => {
                    this.props.done("Documento agregado")
                })

            })
            .catch((error) => {
                console.log(error);
            })
    }
    handleChange = (val) => {
        console.log(val.target.value);
        this.setState({
            documentType: val.target.value,
        }, () => {
            this.changeForm();
        })

    }
    onChangeHandler = event => {
        console.log(event.target.files[0])
        this.setState({
            selectedFile: event.target.files[0],
        })
    }
    changeForm = () => {

        switch (parseInt(this.state.documentType)) {
            case 1:
                return <BookForm required={true} />;
            case 2:
                return <CertificateForm required={true} />;
            case 3:
                return <CovenantForm required={true} />;
            case 4:
                return <EntailmentForm required={true} />;
            case 5:
                return <AcademicStayForm required={true} />;
            case 6:
                return <ResidenceForm required={true} />;
            case 7:
                return <CongressForm required={true} />;
            case 8:
                return <SymposiumForm required={true} />;
            case 9:
                return <Colloquium required={true} />;
            case 10:
                return <JCR_1 required={true} />;
            case 11:
                return <JCR_2 required={true} />;
            case 12:
                return <International_paper_1 required={true} />;
            case 13:
                return <International_paper_2 required={true} />;
            case 14:
                return <National_paper_1 required={true} />;
            case 15:
                return <National_paper_2 required={true} />;
            case 16:
                return <Other required={true} />;
            case 17:


            default:
                return null;


        }

    }
    render() {
        return (

            <Modal show={this.props.show} onHide={this.props.handleClose} animation={true}>
                <Modal.Header closeButton style={{background:"#518ef8"}}>
                    <Modal.Title style={{color:"#fff"}}>Nuevo documento</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    {this.state.loading ? (
                        <CircularProgress />
                    ) : (
                            <div>
                                <Form onSubmit={this.onFormSubmit}>
                                    <Form.Group controlId="exampleForm.ControlSelect1">

                                        <Form.Label>Tipo de documento</Form.Label>
                                        <Form.Control as="select" defaultValue={1} onChange={this.handleChange} required>
                                            {this.loadOptions()}
                                        </Form.Control>
                                    </Form.Group>


                                    {this.changeForm()}
                                    <Form.Group  >
                                        <Form.Label>Subir documento</Form.Label>
                                        <Form.Control type="file" name="file" accept="image/*,.pdf,.xlsx,.xls,.docx,.pptx,.csv" onChange={this.onChangeHandler} required />
                                    </Form.Group>

                                    <Button variant="info" size="small" type="submit" style={{ margin: "0 auto", marginTop: 50, display: "block" }}>
                                        Guardar
                                    </Button>
                                </Form>
                            </div>
                        )}
                </Modal.Body>

            </Modal>

        );
    }
};

export default FormModal;


