import React from 'react';

import 'react-dropzone-uploader/dist/styles.css';
import Dropzone from 'react-dropzone-uploader';

const FileUploader = () => {
   
    const handleChangeStatus = ({ meta }, status) => {
        console.log(status, meta)
    }

    const handleSubmit = (files, allFiles) => {
        console.log(files)
        //console.log(files.map(f => f.meta))
        //allFiles.forEach(f => f.remove())
    }

    return (
        <Dropzone
            onChangeStatus={handleChangeStatus}
            maxFiles={1}
            multiple={false}
            onSubmit={handleSubmit}
            styles={{
                dropzone: { width: 400, height: 200 },
                dropzoneActive: { borderColor: 'green' },
            }}
        />
    )
}

export default FileUploader;