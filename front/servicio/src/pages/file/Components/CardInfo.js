import React, { useReducer } from 'react'
import { Card, Row, Col, Button } from 'react-bootstrap';
import { VisibilityOutlined, GetApp, FindInPageOutlined } from '@material-ui/icons';
import "./info.css";
const CardInfo = (props) => {
    const setInfo = () => {
        const { info } = props;
        /* const fileData=Object.keys(info).map((item)=>{
             if(info[item]!=null){
                 return item;
             }
         })*/
        console.log(info.document_type_fk)
        switch (parseInt(info.document_type_fk)) {
            case 1:
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Título:</Col><Col className="info-value">{info.title}</Col></Row>
                        <Row><Col className="info-title">Edición:</Col><Col className="info-value">{info.edition}</Col></Row>
                        <Row> <Col className="info-title">Autor:</Col><Col className="info-value">{info.author}</Col></Row>
                        <Row> <Col className="info-title">Editorial:</Col><Col className="info-value">{info.publisher}</Col> </Row>
                        <Row> <Col className="info-title">Lugar de publicación:</Col><Col className="info-value">{info.publication_place}</Col></Row>
                        <Row> <Col className="info-title">Año:</Col><Col className="info-value">{info.year}</Col></Row>
                        <Row> <Col className="info-title">Páginas:</Col><Col className="info-value">{info.pages}</Col></Row>
                    </Col>
                )
            case 2:
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Certificado:</Col><Col className="info-value">{info.certification_name}</Col></Row>
                        <Row><Col className="info-title">Propietario:</Col><Col className="info-value">{info.owner}</Col></Row>
                        <Row> <Col className="info-title">Quien lo Otorga:</Col><Col className="info-value">{info.who_grants}</Col></Row>
                        <Row> <Col className="info-title">Duración:</Col><Col className="info-value">{info.time}</Col></Row>
                        <Row> <Col className="info-title">Período:</Col><Col className="info-value">{info.period}</Col></Row>
                        <Row> <Col className="info-title">Fecha de inicio:</Col><Col className="info-value">
                            {
                                new Date(info.start_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                        <Row> <Col className="info-title">Fecha de finalización:</Col><Col className="info-value">
                            {
                                new Date(info.finish_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                    </Col>
                )
            case 3:
            case 4:
            case 5:
            case 6:
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Nombre de la compania:</Col><Col className="info-value">{info.company_name}</Col></Row>
                        <Row><Col className="info-title">Proyecto:</Col><Col className="info-value">{info.project_name}</Col></Row>
                        <Row> <Col className="info-title">Colaboradores:</Col><Col className="info-value">{info.collaborators}</Col></Row>
                        <Row> <Col className="info-title">Duración:</Col><Col className="info-value">{info.duration}</Col></Row>
                        <Row> <Col className="info-title">Fecha de inicio:</Col><Col className="info-value">
                            {
                                new Date(info.start_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                        <Row> <Col className="info-title">Fecha de finalización:</Col><Col className="info-value">
                            {
                                new Date(info.finish_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                    </Col>
                )
            case 7:
            case 9:
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Nombre:</Col><Col className="info-value">{info.name}</Col></Row>
                        <Row><Col className="info-title">Expositores:</Col><Col className="info-value">{info.exhibitors}</Col></Row>
                        <Row> <Col className="info-title">Fecha de inicio:</Col><Col className="info-value">
                            {
                                new Date(info.start_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                        <Row> <Col className="info-title">Fecha de finalización:</Col><Col className="info-value">
                            {
                                new Date(info.finish_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                    </Col>
                )
            case 8:
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Tema:</Col><Col className="info-value">{info.tema}</Col></Row>
                        <Row><Col className="info-title">Expositores:</Col><Col className="info-value">{info.exhibitors}</Col></Row>
                        <Row> <Col className="info-title">Fecha de inicio:</Col><Col className="info-value">
                            {
                                new Date(info.start_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>
                        <Row> <Col className="info-title">Fecha de finalización:</Col><Col className="info-value">
                            {
                                new Date(info.finish_date).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric' })
                            }
                        </Col></Row>

                    </Col>
                )
            case 10://JCR1
            case 11://JCR2
            case 12://Articulo internacional primer autor
            case 13://Articulo inter segundo autor
            case 14://Articulo nacional 1 autor
            case 15://Articulo nacional 2 autor
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Título:</Col><Col className="info-value">{info.title}</Col></Row>
                        <Row><Col className="info-title">Año:</Col><Col className="info-value">{info.year}</Col></Row>
                        <Row> <Col className="info-title">Autor(es):</Col><Col className="info-value">{info.author}</Col></Row>
                        <Row> <Col className="info-title">Referencias:</Col><Col className="info-value">{info.reference}</Col></Row>

                    </Col>
                )
            case 16://Otro
                return (
                    <Col md={6} sm={12}>
                        <Row><Col className="info-title">Título:</Col><Col className="info-value">{info.title}</Col></Row>
                        <Row><Col className="info-title">Autor(es):</Col><Col className="info-value">{info.author}</Col></Row>
                        <Row> <Col className="info-title">Descripción:</Col><Col className="info-value">{info.description}</Col></Row>

                    </Col>
                )


            default:
                return null

        }
    }
    const bytesToSize = (bytes) => {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes == 0) return '0 Byte';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
    }
    return (
        <Card className="card-info">
            <Card.Header>
                <Card.Title>Información</Card.Title>
            </Card.Header>
            <Card.Body>
                <Row className="info-body">

                    {setInfo()}
                    <Col md={6} sm={12}>
                        <Row>
                            <Col className="info-title">Nombre del archivo:</Col>
                            <Col>{props.info.document_name}</Col>
                        </Row>
                        <Row>
                            <Col className="info-title">Tipo:</Col>
                            <Col>{props.info.document_type}</Col>
                        </Row>
                        <Row>
                            <Col className="info-title">Tamaño:</Col>
                            <Col>{bytesToSize(props.info.size)} </Col>
                        </Row>
                        <Row>
                            <Col className="info-title">Agregado el:</Col>
                            <Col>{

                                new Date(props.info.upload_date_f).toLocaleDateString("es-US", { day: 'numeric', month: "long", year: 'numeric',weekday:'long',hour:'2-digit',minute:'2-digit' })

                            }</Col>
                        </Row>

                    </Col>
                </Row>

                <Button variant="success" onClick={props.download} size="sm" style={{ float: "right", margin: 5 }}>
                    Descargar <GetApp />
                </Button>
                <Button variant="info" onClick={props.openModal} size="sm" style={{ float: "right", margin: 5 }}>
                    Vista previa <FindInPageOutlined />
                </Button>
            </Card.Body>
        </Card >
    )
}
export default CardInfo