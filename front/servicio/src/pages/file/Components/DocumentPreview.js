import React, { useReducer } from 'react'
import { Modal } from 'react-bootstrap';
import PDFViewer from 'pdf-viewer-reactjs'
import FileViewer from 'react-file-viewer';


const DocumentPreview = (props) => {
    const setPreview = () => {
        const { format_id, url, format_name } = props;
        console.log(props);

        if (format_id == 1) {
            return (
                <PDFViewer
                    navbarOnTop
                    scale={1.5}
                    document={{
                        url: url
                    }}

                />
            )
        } else if (format_id == 2) {
            return (
                <img
                    src={url}
                    width="100%"
                />
            )
        } else {
            return (
                <FileViewer
                    fileType={format_name}
                    filePath={url}
                />
            )
        }
    }
    return (
        <Modal size="lg" show={props.show} onHide={props.handleClose} animation={true}>
            <Modal.Body>
                {setPreview()}
            </Modal.Body>
        </Modal>
    )
}
export default DocumentPreview