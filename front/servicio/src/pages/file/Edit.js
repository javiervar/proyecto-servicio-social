import React from 'react';
import axios from 'axios';
import NavBarComponent from '../navbar/NavBarComponent';


import { Container, Card, Form, Col, Row } from 'react-bootstrap';
import CONSTANT from '../../store/constant';
import CircularProgress from '@material-ui/core/CircularProgress';

import BookForm from "../home/Forms/Book";
import CertificateForm from "../home/Forms/Certificate";
import CovenantForm from "../home/Forms/Covenant";
import ResidenceForm from "../home/Forms/Residence";
import AcademicStayForm from '../home/Forms/AcademicStay';

import EntailmentForm from '../home/Forms/Entailement';
import CongressForm from '../home/Forms/Congress';
import SymposiumForm from '../home/Forms/Symposium';
import Colloquium from '../home/Forms/Colloquium';

import JCR_1 from '../home/Forms/JCR_1';
import JCR_2 from '../home/Forms/JCR_2';
import International_paper_1 from '../home/Forms/International_paper_1';
import International_paper_2 from '../home/Forms/International_paper_2';
import National_paper_1 from '../home/Forms/National_paper_1';
import National_paper_2 from '../home/Forms/National_paper_2';
import Other from '../home/Forms/Other';
import Button from '@material-ui/core/Button';
import ToastAlert from '../../components/ToastAlert';
import Loading from '../../components/Loading'


import "./Components/info.css"
class Edit extends React.Component {
    state = {
        document: [],
        sidebarOpen: false,
        options: [],
        url: "",
        format_id: 0,
        format_name: '',
        loading: true,
        loading2: false,
        openModal: false,
        DocumentType: 0,
        alertStatus: false,
        alertType: 'success',
        alertMsg: 'Operacion exitosa',

    }


    componentWillMount() {

        const userInfo = JSON.parse(localStorage.getItem("userInfo"));
        CONSTANT.TOKEN = userInfo.token;

        this.getFile();

    }


    getFile = () => {
        this.setState({
            loading: true
        })
        const { Id, DocumentType } = this.props.location.state;
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.get(CONSTANT.API_PATH + "/api/document/" + Id + "/" + DocumentType)
            .then((response) => {
                console.log(response);
                const { path, format_fk, format } = response.data[0];
                this.setState({
                    document: response.data[0],
                    DocumentType: DocumentType,
                    loading: false,
                    
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    changeForm = (id) => {

        switch (parseInt(id)) {
            case 1:
                return <BookForm required={true} info={this.state.document} />;
            case 2:
                return <CertificateForm required={true} info={this.state.document} />;
            case 3:
                return <CovenantForm required={true} info={this.state.document} />;
            case 4:
                return <EntailmentForm required={true} info={this.state.document} />;
            case 5:
                return <AcademicStayForm required={true} info={this.state.document} />;
            case 6:
                return <ResidenceForm required={true} info={this.state.document} />;
            case 7:
                return <CongressForm required={true} info={this.state.document} />;
            case 8:
                return <SymposiumForm required={true} info={this.state.document} />;
            case 9:
                return <Colloquium required={true} info={this.state.document} />;
            case 10:
                return <JCR_1 required={true} info={this.state.document} />;
            case 11:
                return <JCR_2 required={true} info={this.state.document} />;
            case 12:
                return <International_paper_1 required={true} info={this.state.document} />;
            case 13:
                return <International_paper_2 required={true} info={this.state.document} />;
            case 14:
                return <National_paper_1 required={true} info={this.state.document} />;
            case 15:
                return <National_paper_2 required={true} info={this.state.document} />;
            case 16:
                return <Other required={true} info={this.state.document} />;
            case 17:


            default:
                return null;


        }

    }

    alertHandle = () => {
        this.setState({
            alertStatus: !this.state.alertStatus
        })
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        const { Id, DocumentType } = this.props.location.state;
        const data = new FormData(event.target);
        const document = Object.fromEntries(data);

        document.document_type = DocumentType;
        document.document_fk = Id;

        this.setState({
            loading: true,
        })


        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/update-document", document)
            .then((response) => {
                let res = response.data;
                console.log(res)
                if (!res.error) {
                    console.log("entro")
                    this.setState({
                        alertMsg: "Información actualizada con éxito",
                        alertType: "success"
                    }, () => {
                        this.alertHandle();
                        this.getFile();
                    })

                } else {
                    this.setState({
                        alertMsg: "Hubo un problema al actualizar",
                        alertType: "error"
                    }, () => {
                        this.alertHandle();
                        this.getFile();
                    })

                }


            })
            .catch((error) => {
                console.log(error);
            })

    }

    onFormSubmitUpdate = (event) => {
        event.preventDefault();
        const data = new FormData(event.target);
        const document = Object.fromEntries(data);
        this.setState({
            loading2: true
        })

        //SAVE FILE
        const dataFile = new FormData()
        dataFile.append('document', document.file);

        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/upload-document", dataFile)
            .then((response) => {
                const resp = response.data.data;
                this.updateDocument(resp, document)

            })
            .catch((error) => {
                console.log(error);
            })
    }

    updateDocument = (data, document) => {
        const { Id, DocumentType } = this.props.location.state;
        var format = 2;//image
        if (data.mimetype == "application/pdf") {
            format = 1
        } else if (data.mimetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
            format = 3;
        } else if (data.mimetype == "application/vnd.ms-excel"||data.mimetype=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
            format = 4;
        } else if (data.mimetype == "text/csv") {
            format = 5;
        }
        data.format_fk = format;
        data.id = Id;

        console.log("-->", data)
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.post(CONSTANT.API_PATH + "/api/update-file", data)
            .then((response) => {
                console.log(response)
                let res = response.data;
                console.log(res)
                if (!res.error) {
                    this.setState({
                        alertMsg: "Información actualizada",
                        alertType: "success",
                        loading2: false
                    }, () => {
                        this.alertHandle();
                        
                    })

                } else {
                    this.setState({
                        alertMsg: "Hubo un problema al actualizar",
                        alertType: "error",
                        loading2: false
                    }, () => {
                        this.alertHandle();
                    })

                }


            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
        return (
            <div>
                <NavBarComponent />
                <ToastAlert open={this.state.alertStatus} handleClose={this.alertHandle} alertType={this.state.alertType} msg={this.state.alertMsg} />
                <div className="info-edit">
                    <Row>

                        <Col>

                            <Card>
                                <Card.Header>
                                    <Card.Title>
                                        Informacion
                                </Card.Title>
                                </Card.Header>
                                <Card.Body >
                                    {this.state.loading ? (
                                        <img src={require("../../assets/loadings/loading8.gif")} style={{ width: "50%", display: "block", margin: "0 auto" }} />
                                    ) : (
                                            <Form onSubmit={this.onFormSubmit}>

                                                {this.changeForm(this.state.DocumentType)}

                                                <Button type="submit" variant="contained" style={{ background: "#009688", color: "#fff" }} >
                                                    Actualizar
                                                </Button>

                                            </Form>
                                        )}


                                </Card.Body>

                            </Card>
                        </Col>
                        <Col>
                            <Card>
                                <Card.Header>
                                    <Card.Title>
                                        Documento
                                </Card.Title>
                                </Card.Header>
                                <Card.Body >

                                    <Form onSubmit={this.onFormSubmitUpdate}>
                                        {this.state.loading2 ? (
                                            <CircularProgress style={{ display: 'block', margin: '0 auto', color: "rgb(33, 150, 243)" }} />
                                        ) : (
                                                <Form.Group  >
                                                    <Form.Label>Subir documento</Form.Label>
                                                    <Form.Control type="file" name="file" onChange={this.onChangeHandler} required />
                                                </Form.Group>
                                            )}
                                        <Button type="submit" variant="contained" style={{ background: "#009688", color: "#fff" }} >
                                            Actualizar
                                        </Button>

                                    </Form>


                                </Card.Body>

                            </Card>
                        </Col>


                    </Row>
                </div>

            </div>
        );
    }
};

export default Edit;


