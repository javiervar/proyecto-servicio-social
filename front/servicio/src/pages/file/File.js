import React from 'react';
import api from '../../api/config';
import axios from 'axios';
import NavBarComponent from '../navbar/NavBarComponent';
import CardInfo from './Components/CardInfo'
import DocumentPreview from './Components/DocumentPreview'

import { Container, Card, Row, Col, Button } from 'react-bootstrap';
import CONSTANT from '../../store/constant';
import CircularProgress from '@material-ui/core/CircularProgress';


class File extends React.Component {
    state = { document: [], sidebarOpen: false, options: [], url: "", format_id: 0,format_name:'', loading: true, openModal: false }


    componentWillMount() {

        const userInfo = JSON.parse(localStorage.getItem("userInfo"));
        CONSTANT.TOKEN = userInfo.token;

        this.getFile();

    }


    getFile = () => {
        const { Id,DocumentType } = this.props.location.state;
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['x-access-token'] = CONSTANT.TOKEN;

        axios.get(CONSTANT.API_PATH + "/api/document/" + Id+"/"+DocumentType)
            .then((response) => {
                console.log(response);
                const { path, format_fk,format } = response.data[0];
                this.setState({
                    document: response.data[0],
                    loading: false,
                    url: CONSTANT.API_PATH + "/view/" + Id,
                    format_id: format_fk,
                    format_name:format
                })
            })
            .catch((error) => {
                console.log(error);
            })
    }

    openModal = () => {
        this.setState({
            openModal: !this.state.openModal
        })
    }

    downloadFile=()=>{
        const { Id } = this.props.location.state;
        var download_link=CONSTANT.API_PATH + "/download/" + Id;
        window.location.replace(download_link);
    }


    render() {
        return (
            <div>
                <NavBarComponent />
                
                <DocumentPreview
                    url={this.state.url}
                    format_id={this.state.format_id}
                    format_name={this.state.format_name}
                    show={this.state.openModal}
                    handleClose={this.openModal} />

                <Container>
                    {this.state.loading ? (
                        <CircularProgress />
                    ) : (
                            <CardInfo info={this.state.document} download={this.downloadFile} openModal={this.openModal}/>

                        )}

                </Container>

            </div>
        );
    }
};

export default File;


