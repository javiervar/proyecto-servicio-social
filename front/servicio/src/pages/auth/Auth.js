import React from 'react';

import axios from 'axios';
import { Container, Row, Form, Button } from 'react-bootstrap';
import CONSTANT from '../../store/constant';
import "./auth.css";



class Auth extends React.Component {
    state = {
        email: '',
        password: '',
    }

    componentDidMount() {
        localStorage.clear();
    }

    login = (e) => {
        e.preventDefault();
        
        const credentials = {email: this.state.email, password: this.state.password};
        console.log(credentials)
        axios.post(CONSTANT.API_PATH+"/api/login",credentials).then(res => {
            console.log(res);
            if(res.data.auth){
                console.log("entro")
                localStorage.setItem("userInfo", JSON.stringify(res.data));
                this.props.history.push('/home');
            }else {
                console.log("error");
            }
        });
    };

    onChange = (e) =>this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <div className="page">
                <Form className="panel" onSubmit={this.login}>
                    <img src={require("../../assets/img/user.png")} className="logo" />

                    <Form.Group >
                        <Form.Control type="email" name="email" placeholder="Correo"  onChange={this.onChange} required/>

                    </Form.Group>

                    <Form.Group >
                        <Form.Control type="password" name="password" placeholder="Contraseña"  onChange={this.onChange} required/>
                    </Form.Group>

                    <Button variant="success" type="submit" block>
                        Iniciar sesión
                </Button>
                </Form>
            </div>


        );
    }
};

export default Auth;


