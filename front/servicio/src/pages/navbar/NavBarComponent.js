import React from 'react';
import { Dropdown, Navbar, Form } from 'react-bootstrap';
import { useHistory } from "react-router-dom";



export default function NavbarComponent(props) {
    

    const history = useHistory();

    const redirect = () => {
        history.push("/home");
    }


    return (
        <Navbar bg="default" expand="lg">
            <Navbar.Brand  onClick={()=>redirect()} style={{cursor:"pointer"}}>
                <img src={require("../../assets/img/logo1.png")} width={40} />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />

            <Navbar.Collapse className="justify-content-end">
                <Form inline>
                    <Dropdown alignRight>
                        <Dropdown.Toggle variant="default" id="dropdown-basic">
                            <i className="fas fa-sliders-h"></i>
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-2"><i className="fas fa-users-cog"></i> Usuarios</Dropdown.Item>
                            <Dropdown.Item href="#/action-1"><i className="fas fa-sign-out-alt"></i> Cerrar sesion</Dropdown.Item>

                        </Dropdown.Menu>
                    </Dropdown>
                </Form>
            </Navbar.Collapse>
        </Navbar>
    )

}

