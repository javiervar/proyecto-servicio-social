const express = require('express');
const router = express.Router();
const DocumentController = require('../controller/DocumentController');
const DocumentTypeController = require('../controller/DocumentTypeController');
const AuthController = require('../controller/AuthController');

const VerifyToken = require('../auth/VerifyToken');

//auth
router.post('/api/register', AuthController.register);
router.post('/api/login', AuthController.login);
//documents
router.get("/api/documents", VerifyToken, DocumentController.getDocuments);
router.get("/api/document/:id/:type", VerifyToken, DocumentController.getDocument);
router.get("/api/search", VerifyToken, DocumentController.searchDocument);
router.post('/api/upload-document', VerifyToken, DocumentController.uploadDocument);
router.post('/api/save-file', VerifyToken, DocumentController.saveDocumentFile);
router.post('/api/update-file', VerifyToken, DocumentController.updateFileDocument);

router.post('/api/save-document', VerifyToken, DocumentController.saveDocument);
router.post('/api/update-document', VerifyToken, DocumentController.updateDocument);
//delete
router.delete('/api/delete', VerifyToken, DocumentController.deleteDocument);




//type of documents
router.get("/api/document_type_item", VerifyToken, DocumentTypeController.documentTypeItem);
//Viewer
router.get("/view/:id", DocumentController.viewDocument);
router.get("/download/:id", DocumentController.downloadDocument);


module.exports = router;