const express = require('express')
const mysqlConnection = require("../db/db");
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../auth/config');

var AuthController = {};


AuthController.register = (req, res) => {
    const { name, password, email } = req.body;
    var hashedPassword = bcrypt.hashSync(password, 8);

    var values = [
        [name, hashedPassword, email]
    ]
    var query = "insert into user(name,password,email) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            var token = jwt.sign({ id: rows.insertId }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.json({ auth: true, token: token, id: rows.insertId });
        } else {
            console.log(err)
            var error = {
                error: 1,
                msg: "There was a problem registering the user."
            }
            return res.json(error);
        }
    })
}

AuthController.login = (req, res) => {
    const { password, email } = req.body;

    var query = "select * from user where email=?";
    mysqlConnection.query(query, [email], (err, rows, fields) => {

        if (rows.length == 0) {
            return res.json({
                error: 1,
                msg: 'No user found.'
            })
        }

        if (!err) {
            var passwordIsValid = bcrypt.compareSync(password, rows[0].password);
            if (!passwordIsValid) return res.json({ error: 2, msg: "Wrong password", auth: false, });
            var token = jwt.sign({ id: rows[0].user_id }, config.secret, {
                expiresIn: 86400 // expires in 24 hours
            });

            res.json({ error: 0, auth: true, token: token, id: rows[0].user_id });
        } else {
            console.log(err)
            var error = {
                error: 2,
                msg: 'Error on the server.'
            }
            return res.json(error);
        }
    });
}

module.exports = AuthController;