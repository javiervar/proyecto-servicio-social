const express = require('express')
const mysqlConnection = require("../db/db");
const fs = require('fs');

var DocumentController = {};

const pages = (query, pag, callback) => {
    mysqlConnection.query(query, callback)
}


DocumentController.getDocuments = (req, res) => {
    const params = req.query;
    console.log(params);
    var pag = params.page * 20;
    var query = "  select *,DATE_FORMAT(doc.upload_date, '%d %M %Y') as upload_date_f,docty.description as document_type from document doc left join book boo on boo.document_fk=doc.document_id";
    query += "  left join document_type docty on docty.document_type_id=doc.document_type_fk";
    query += "  left join certificate cert on cert.document_fk=doc.document_id left join covenant cov on";
    query += "  cov.document_fk=doc.document_id left join entailment ent on ent.document_fk=doc.document_id";
    query += "  left join residence aca on aca.document_fk=doc.document_id order by doc.upload_date DESC"

    mysqlConnection.query(query, (err, rows, fields) => {
        let paginas = Math.ceil(rows.length / 20);
        console.log(paginas);
        query += " LIMIT " + pag + ",20";

        mysqlConnection.query(query, (err, rows, fields) => {
            if (!err) {
                var obj = {
                    pages: paginas,
                    data: rows
                }
                res.json(obj);
            } else {
                console.log(err)
            }
        })
    });

}

DocumentController.getDocument = (req, res) => {
    const { id, type } = req.params;
    console.log(req.params);
    var query = "select *,DATE_FORMAT(doc.upload_date, '%d %M %Y %r') as upload_date_f,docty.description as document_type from document doc ";
    query += " left join document_type docty on docty.document_type_id=doc.document_type_fk ";
    query += " left join format form on form.format_id=doc.format_fk ";
    switch (parseInt(type)) {
        case 1:
            query += "left join book boo on boo.document_fk=doc.document_id"
            break;
        case 2:
            query += "left join certificate cert on cert.document_fk=doc.document_id";
            break;
        case 3:
            query += "left join covenant cov on cov.document_fk=doc.document_id";
            break;
        case 4:
            query += "left join entailment ent on ent.document_fk=doc.document_id";
            break;
        case 5:
            query += "left join academic_stay aca on aca.document_fk=doc.document_id";
            break;
        case 6:
            query += "left join residence res on res.document_fk=doc.document_id";
            break;
        case 7:
            query += "left join congress cong on cong.document_fk=doc.document_id";
            break;
        case 8:
            query += "left join symposium sym on sym.document_fk=doc.document_id";
            break;
        case 9:
            query += "left join colloquium col on col.document_fk=doc.document_id";
            break;
        case 10:
            query += "left join jcr_first jcr on jcr.document_fk=doc.document_id";
            break;
        case 11:
            query += "left join jcr_second jcr on jcr.document_fk=doc.document_id";
            break;
        case 12:
            query += "left join international_paper1 inter on inter.document_fk=doc.document_id";
            break;
        case 13:
            query += "left join international_paper2 inter on inter.document_fk=doc.document_id";
            break;
        case 14:
            query += "left join national_paper1 nat on nat.document_fk=doc.document_id";
            break;
        case 15:
            query += "left join national_paper2 nat on nat.document_fk=doc.document_id";
            break;
        case 16:
            query += "left join other_document other on other.document_fk=doc.document_id";
            break;
    }

    query += " where doc.document_id = ? ";


    mysqlConnection.query(query, [id], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
            return res.json({
                error: 1,
                msg: 'Query error',

            })
        }
    })
}



DocumentController.searchDocument = (req, res) => {
    var params = req.query;
    console.log("txt:" + params.txt);
    const { upload_date, document_type, txt, page } = params;
    var pag = page * 20;
    let whereExist = false;

    var filter = { firstAnd: false };
    var query = "select *,DATE_FORMAT(doc.upload_date, '%d %M %Y') as upload_date_f,docty.description as document_type from document doc left join ";
    query += "book boo on boo.document_fk=doc.document_id";
    query += "  left join document_type docty on docty.document_type_id=doc.document_type_fk";
    query += "  left join certificate cert on cert.document_fk=doc.document_id left join covenant cov on";
    query += "  cov.document_fk=doc.document_id left join entailment ent on ent.document_fk=doc.document_id";
    query += "  left join academic_stay aca on aca.document_fk=doc.document_id"
    query += "  left join residence res on res.document_fk=doc.document_id"
    query += "  left join congress cong on cong.document_fk=doc.document_id"
    if (txt !== undefined) {
        query += "  where doc.document_name LIKE '" + txt + "%'";
        whereExist = true;
    }


    if (document_type > -1) {
        const { title, author, year, edition, publisher, publication_place, owner, start_date, finish_date, who_grants, time, certification_name, period, company_name, duration, project_name, collaborators, } = params;

        query += whereExist ? " OR(" : " where";
        switch (document_type) {

            case '1'://LIBRO
                if (title != "") {
                    query += search_first(filter);
                    query += " boo.title LIKE '" + title + "%'";
                }
                if (author != "") {
                    query += search_first(filter);
                    query += " boo.author LIKE '" + author + "%'";
                }

                if (year != "") {
                    query += search_first(filter);
                    query += " boo.year LIKE '" + year + "%'";
                }

                if (edition != "") {
                    query += search_first(filter);
                    query += " boo.edition LIKE '" + edition + "%'";
                }

                if (publisher != "") {
                    query += search_first(filter);
                    query += " boo.publisher LIKE '" + publisher + "%'";
                }

                if (publication_place != "") {
                    query += search_first(filter);
                    query += " boo.publication_place LIKE '" + publication_place + "%'";
                }


                break;
            case '2'://Certificado
                if (owner != "") {
                    query += search_first(filter);
                    query += " cert.owner LIKE '" + owner + "%'";
                }
                if (start_date != "") {
                    query += search_first(filter);
                    query += " cert.start_date LIKE '" + start_date + "%'";
                }
                if (finish_date != "") {
                    query += search_first(filter);
                    query += " cert.finish_date LIKE '" + finish_date + "%'";
                }
                if (who_grants != "") {
                    query += search_first(filter);
                    query += " cert.who_grants LIKE '" + who_grants + "%'";
                }
                if (time != "") {
                    query += search_first(filter);
                    query += " cert.time LIKE '" + time + "%'";
                }
                if (certification_name != "") {
                    query += search_first(filter);
                    query += " cert.certification_name LIKE '" + certification_name + "%'";
                }
                if (period != "") {
                    query += search_first(filter);
                    query += " cert.period LIKE '" + period + "%'";
                }
                break;
            case '3'://Convenio
                if (company_name != "") {
                    query += search_first(filter);
                    query += " cov.company_name LIKE '" + company_name + "%'";
                }
                if (duration != "") {
                    query += search_first(filter);
                    query += " cov.duration LIKE '" + duration + "%'";
                }
                if (project_name != "") {
                    query += search_first(filter);
                    query += " cov.project_name LIKE '" + project_name + "%'";
                }
                if (collaborators != "") {
                    query += search_first(filter);
                    query += " cov.collaborators LIKE '" + collaborators + "%'";
                }
                if (start_date != "") {
                    query += search_first(filter);
                    query += " cov.start_date LIKE '" + start_date + "%'";
                }
                if (finish_date != "") {
                    query += search_first(filter);
                    query += " cov.finish_date LIKE '" + finish_date + "%'";
                }
                break;
            case '4'://Vinculacion
                console.log("search by vinculacion")
                if (company_name != "") {
                    query += search_first(filter);
                    query += " ent.company_name LIKE '" + company_name + "%'";
                }
                if (duration != "") {
                    query += search_first(filter);
                    query += " ent.duration LIKE '" + duration + "%'";
                }
                if (project_name != "") {
                    query += search_first(filter);
                    query += " ent.project_name LIKE '" + project_name + "%'";
                }
                if (collaborators != "") {
                    query += search_first(filter);
                    query += " ent.collaborators LIKE '" + collaborators + "%'";
                }
                if (start_date != "") {
                    query += search_first(filter);
                    query += " ent.start_date LIKE '" + start_date + "%'";
                }
                if (finish_date != "") {
                    query += search_first(filter);
                    query += " ent.finish_date LIKE '" + finish_date + "%'";
                }
                break;
            case '5':// Estancia academica
                console.log("search by estancia academica || residencia")
                if (company_name != "") {
                    query += search_first(filter);
                    query += " aca.company_name LIKE '" + company_name + "%'";
                }
                if (duration != "") {
                    query += search_first(filter);
                    query += " aca.duration LIKE '" + duration + "%'";
                }
                if (project_name != "") {
                    query += search_first(filter);
                    query += " aca.project_name LIKE '" + project_name + "%'";
                }
                if (collaborators != "") {
                    query += search_first(filter);
                    query += " aca.collaborators LIKE '" + collaborators + "%'";
                }
                if (start_date != "") {
                    query += search_first(filter);
                    query += " aca.start_date LIKE '" + start_date + "%'";
                }
                if (finish_date != "") {
                    query += search_first(filter);
                    query += " aca.finish_date LIKE '" + finish_date + "%'";
                }
                break;
            case '7':// Congreso
                console.log("search by congreso")
                if (name != "") {
                    query += search_first(filter);
                    query += " cong.name LIKE '" + company_name + "%'";
                }
                if (exhibitors != "") {
                    query += search_first(filter);
                    query += " cong.exhibitors LIKE '" + collaborators + "%'";
                }
                if (start_date != "") {
                    query += search_first(filter);
                    query += " cong.start_date LIKE '" + start_date + "%'";
                }
                if (finish_date != "") {
                    query += search_first(filter);
                    query += " cong.finish_date LIKE '" + finish_date + "%'";
                }
                break;


        }

        query += whereExist ? " )" : " ";
        query += search_first(filter);
        query += " doc.document_type_fk=" + document_type;
    }

    query += " order by doc.upload_date DESC";
    console.log(query);
    mysqlConnection.query(query, (err, rows, fields) => {
        if (err) {
            var obj = {
                error: 1,
                msg: "Error :" + JSON.stringify(err)
            }
            res.json(obj);
            return;
        }
        let paginas = Math.ceil(rows.length / 25);
        console.log(paginas);
        query += " LIMIT " + pag + ",25";
        console.log(query);
        mysqlConnection.query(query, (err, rows, fields) => {
            if (!err) {
                var obj = {
                    error: 0,
                    pages: paginas,
                    data: rows
                }
                res.json(obj);
            } else {
                var obj = {
                    error: 1,
                    msg: "Error :" + JSON.stringify(err)
                }
                res.json(obj);
            }
        });
    })
}

DocumentController.saveDocumentFile = (req, res) => {
    const { path, type, name, size, mimetype, format_fk } = req.body;
    var values = [
        [path, type, name, size, mimetype, format_fk]
    ]
    var query = "insert into document(path,document_type_fk,document_name,size,mimetype,format_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveDocument = (req, res) => {
    const { document_type } = req.body;
    switch (parseInt(document_type)) {
        case 1:
            DocumentController.saveBook(req, res);
            break;
        case 2:
            DocumentController.saveCertificate(req, res);
            break;
        case 3:
            DocumentController.saveCoventant(req, res);
            break;
        case 4:
            DocumentController.saveEntailment(req, res);
            break;
        case 5:
            DocumentController.saveAcademicStay(req, res);
            break;
        case 6:
            DocumentController.saveResidence(req, res);
            break;
        case 7:
            DocumentController.saveCongress(req, res);
            break;
        case 8:
            DocumentController.saveSymposium(req, res);

            break;
        case 9:
            DocumentController.saveColloquium(req, res);

            break;

        case 10:
            DocumentController.saveJcr_first(req, res);

            break;

        case 11:
            DocumentController.saveJcr_second(req, res);

            break;

        case 12:
            DocumentController.saveInternational_paper1(req, res);

            break;
        case 13:
            DocumentController.saveInternational_paper2(req, res);

            break;
        case 14:
            DocumentController.saveNational_paper1(req, res);

            break;
        case 15:
            DocumentController.saveNational_paper2(req, res);

            break;

        case 16:
            DocumentController.saveOther_document(req, res);

            break;
    }
}

DocumentController.updateDocument = (req, res) => {
    const { document_type } = req.body;
    switch (parseInt(document_type)) {
        case 1:
            DocumentController.updateBook(req, res);
            break;
        case 2:
            DocumentController.updateCertificate(req, res);
            break;
        case 3:
            DocumentController.updateCoventant(req, res);
            break;
        case 4:
            DocumentController.updateEntailment(req, res);
            break;
        case 5:
            DocumentController.updateAcademicStay(req, res);
            break;
        case 6:
            DocumentController.updateResidence(req, res);
            break;
        case 7:
            DocumentController.updateCongress(req, res);
            break;
        case 8:
            DocumentController.updateSymposium(req, res);

            break;
        case 9:
            DocumentController.updateColloquium(req, res);

            break;

        case 10:
            DocumentController.updateJcr_first(req, res);

            break;

        case 11:
            DocumentController.updateJcr_second(req, res);

            break;

        case 12:
            DocumentController.updateInternational_paper1(req, res);

            break;
        case 13:
            DocumentController.updateInternational_paper2(req, res);

            break;
        case 14:
            DocumentController.updateNational_paper1(req, res);

            break;
        case 15:
            DocumentController.updateNational_paper2(req, res);

            break;

        case 16:
            DocumentController.updateOther_document(req, res);

            break;
    }
}
/**
 * ############################# SAVE ########################
 */

DocumentController.saveBook = (req, res) => {
    //document_id=1
    const { title, author, year, edition, pages, publisher, publication_place, document_fk } = req.body;
    var values = [
        [title, author, year, edition, pages, publisher, publication_place, document_fk]
    ]
    var query = "insert into book(title, author, year, edition,pages, publisher,publication_place,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveCertificate = (req, res) => {
    //document_id=2
    const { owner, start_date, finish_date, who_grants, time, certification_name, period, document_fk } = req.body;
    var values = [
        [owner, start_date, finish_date, who_grants, time, certification_name, period, document_fk]
    ]
    var query = "insert into certificate(owner,start_date,finish_date,who_grants,time,certification_name,period,document_fk ) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveCoventant = (req, res) => {
    //document_id=3
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [
        [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]
    ]
    var query = "insert into covenant(company_name,duration,project_name,collaborators,start_date,finish_date,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveEntailment = (req, res) => {
    //document_id=4
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [
        [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]
    ]
    var query = "insert into entailment(company_name,duration,project_name,collaborators,start_date,finish_date,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveAcademicStay = (req, res) => {
    //document_id=5
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [
        [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]
    ]
    var query = "insert into academic_stay(company_name,duration,project_name,collaborators,start_date,finish_date,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveResidence = (req, res) => {
    //document_id=6
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [
        [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]
    ]
    var query = "insert into residence(company_name,duration,project_name,collaborators,start_date,finish_date,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveCongress = (req, res) => {
    //document_id=7
    const { name, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [
        [name, start_date, finish_date, exhibitors, document_fk]
    ]
    var query = "insert into congress(name,start_date,finish_date,exhibitors,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveSymposium = (req, res) => {
    //document_id=8
    const { tema, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [
        [tema, start_date, finish_date, exhibitors, document_fk]
    ]
    var query = "insert into symposium(tema,start_date,finish_date,exhibitors,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveColloquium = (req, res) => {
    //document_id=9
    const { name, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [
        [name, start_date, finish_date, exhibitors, document_fk]
    ]
    var query = "insert into colloquium(name,start_date,finish_date,exhibitors,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveJcr_first = (req, res) => {
    //document_id=10
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into jcr_first(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveJcr_second = (req, res) => {
    //document_id=11
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into jcr_second(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveInternational_paper1 = (req, res) => {
    //document_id=12
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into international_paper1(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveInternational_paper2 = (req, res) => {
    //document_id=13
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into international_paper2(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveNational_paper1 = (req, res) => {
    //document_id=14
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into national_paper1(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveNational_paper2 = (req, res) => {
    //document_id=15
    const { title, year, author, reference, document_fk } = req.body;
    var values = [
        [title, year, author, reference, document_fk]
    ]
    var query = "insert into national_paper2(title,year,author,reference,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

DocumentController.saveOther_document = (req, res) => {
    //document_id=16
    const { title, author, description, document_fk } = req.body;
    var values = [
        [title, author, description, document_fk]
    ]
    var query = "insert into other_document(title,author,description,document_fk) values ?";

    mysqlConnection.query(query, [values], (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}

/** 
 * #################### UPDATE ###############
 *
 * */
DocumentController.updateBook = (req, res) => {
    //document_id=1
    const { title, author, year, edition, pages, publisher, publication_place, document_fk } = req.body;
    let values = [title, author, year, edition, pages, publisher, publication_place, document_fk]
    console.log(values)
    var query = "update book set title=?, author=?, year=?, edition=?,pages=?, publisher=?,publication_place=? where document_fk=?";


    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            });
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            });
        }
    })
}

DocumentController.updateCertificate = (req, res) => {
    //document_id=2
    const { owner, start_date, finish_date, who_grants, time, certification_name, period, document_fk } = req.body;
    var values = [owner, start_date, finish_date, who_grants, time, certification_name, period, document_fk]

    var query = "update certificate set owner=?, start_date=?, finish_date=?, who_grants=?, time=?, certification_name=?, period=?  where document_fk=?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateCoventant = (req, res) => {
    //document_id=3
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]

    var query = "update covenant set company_name=?, duration=?, project_name=?, collaborators=?, start_date=?, finish_date=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateEntailment = (req, res) => {
    //document_id=4
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]

    var query = "update entailment set company_name=?, duration=?, project_name=?, collaborators=?, start_date=?, finish_date=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}


DocumentController.updateAcademicStay = (req, res) => {
    //document_id=5
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]

    var query = "update academic_stay set company_name=?, duration=?, project_name=?, collaborators=?, start_date=?, finish_date=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateResidence = (req, res) => {
    //document_id=6
    const { company_name, duration, project_name, collaborators, start_date, finish_date, document_fk } = req.body;
    var values = [company_name, duration, project_name, collaborators, start_date, finish_date, document_fk]

    var query = "update residence set company_name=?, duration=?, project_name=?, collaborators=?, start_date=?, finish_date=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateCongress = (req, res) => {
    //document_id=7
    const { name, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [name, start_date, finish_date, exhibitors, document_fk]

    var query = "update congress set name=?, start_date=?, finish_date=?, exhibitors=? where  document_fk = ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateSymposium = (req, res) => {
    //document_id=8
    const { tema, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [tema, start_date, finish_date, exhibitors, document_fk]

    var query = "update symposium set tema=?, start_date=?, finish_date=?, exhibitors=?  where document_fk = ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateColloquium = (req, res) => {
    //document_id=9
    const { name, start_date, finish_date, exhibitors, document_fk } = req.body;
    var values = [name, start_date, finish_date, exhibitors, document_fk]

    var query = "update colloquium set name=?, start_date=?, finish_date=?, exhibitors=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateJcr_first = (req, res) => {
    //document_id=10
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk];

    var query = "update jcr_first set title=?, year=?, author=?, reference=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateJcr_second = (req, res) => {
    //document_id=11
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk];

    var query = "update jcr_second set title=?, year=?, author=?, reference=?  where document_fk= ?";


    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateInternational_paper1 = (req, res) => {
    //document_id=12
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk]


    var query = "update  international_paper1 set title=?, year=?, author=?, reference=?  where document_fk= ?";


    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateInternational_paper2 = (req, res) => {
    //document_id=13
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk]


    var query = "update  international_paper2 set title=?, year=?, author=?, reference=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateNational_paper1 = (req, res) => {
    //document_id=14
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk];

    var query = "update  national_paper1 set title=?, year=?, author=?, reference=?  where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateNational_paper2 = (req, res) => {
    //document_id=15
    const { title, year, author, reference, document_fk } = req.body;
    var values = [title, year, author, reference, document_fk]

    var query = "update national_paper2 set title=?, year=?, author=?, reference=? where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

DocumentController.updateOther_document = (req, res) => {
    //document_id=16
    const { title, author, description, document_fk } = req.body;
    var values = [title, author, description, document_fk]

    var query = "update other_document set title=?,author=?,description=? where document_fk= ?";

    mysqlConnection.query(query, values, (err, rows, fields) => {
        if (!err) {
            res.json({
                error: false,
                msg: rows
            })
        } else {
            res.json({
                error: true,
                msg: JSON.stringify(err)
            })
        }
    })
}

/**
 * ############### UPLLOAD, DELETE, UPDARE,VIEW,DOWNLOAD FILE DOCUMENT #####################
 * 
 */
DocumentController.uploadDocument = async (req, res) => {
    try {
        if (!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            let document = req.files.document;
            console.log(document)
            var randomPath = randomPathId(20);
            var path_name = randomPath + "_" + document.name;

            document.mv("./statics/files/" + path_name);
            //send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: document.name,
                    path: path_name,
                    mimetype: document.mimetype,
                    size: document.size
                }
            });
        }
    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
}

const deleteFile = async (id) => {
    try {
        console.log("Archivo a borrar :", id)
        var query = "select path from document where document_id=" + id;

        mysqlConnection.query(query, (err, rows, fields) => {
            if (!err) {
                let path = "./statics/files/" + rows[0].path;
                console.log(path);
                fs.unlinkSync(path);

            } else {
                console.log(err)
            }
        })

    } catch (err) {
        console.log(err)

    }
}

DocumentController.updateFileDocument = async (req, res) => {
    try {
        const { path, name, size, mimetype, format_fk, id } = req.body;
        var values = [
            path, name, size, mimetype, format_fk, id
        ]

        deleteFile(id);
        var query = "update  document set path=?, document_name=?, size=?,mimetype=?,format_fk=? where document_id=?";

        mysqlConnection.query(query, values, (err, rows, fields) => {
            if (!err) {
                res.json({
                    error: false,
                    msg: rows
                })
            } else {
                res.json({
                    error: true,
                    msg: JSON.stringify(err)
                })
            }
        })

    } catch (err) {
        console.log(err)
        res.status(500).send(err);
    }
}

DocumentController.viewDocument = (req, res) => {
    const { id } = req.params;
    var query = "select path,mimetype from document where document_id=?";
    mysqlConnection.query(query, [id], (err, rows, fields) => {
        if (!err) {
            const { path, mimetype } = rows[0];
            var filePath = "./statics/files/" + path;
            fs.readFile(filePath, (err, data) => {
                res.contentType(mimetype);
                res.send(data);
            });

        } else {
            res.json({
                error: 1,
                msg: 'Query error'
            })
        }
    })
}
DocumentController.deleteDocument = (req, res) => {
    const { id } = req.body;
    deleteFile(id);
    console.log(id)
    var query = "delete from document where document_id=?";
    mysqlConnection.query(query, [id], (err, rows, fields) => {
        if (!err) {
            res.json({
                error: 0,
                msg: rows
            })
        } else {

            res.json({
                error: 1,
                msg: 'Query error'
            })
        }
    })
}

DocumentController.downloadDocument = (req, res) => {
    const { id } = req.params;
    var query = "select path,document_name from document where document_id=?";
    mysqlConnection.query(query, [id], (err, rows, fields) => {
        if (!err) {
            const { path, document_name } = rows[0];
            var filePath = "./statics/files/" + path;
            res.download(filePath, document_name);

        } else {
            res.json({
                error: 1,
                msg: 'Query error'
            })
        }
    })
}




function search_first(filter) {
    console.log(filter)

    if (filter.firstAnd) {
        return " AND";
    } else {
        filter.firstAnd = true;
        return " ";
    }


}

function randomPathId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}



module.exports = DocumentController;