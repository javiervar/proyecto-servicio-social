const express = require('express')
const mysqlConnection = require("../db/db");

var DocumentTypeController = {};


DocumentTypeController.documentTypeItem = (req, res) => {
    var query = "select description as label,document_type_id as value from document_type ORDER BY description";
    
    mysqlConnection.query(query, (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err)
        }
    })
}



module.exports = DocumentTypeController;