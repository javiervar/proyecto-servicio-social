const express=require('express');
const app=express();
const fileUpload = require('express-fileupload');

//settings
app.set('port',process.env.PORT||8000);

//midleware
app.use(express.json());


app.use(function(req, res, next) {
    var oneof = false;
    if(req.headers.origin) {
        res.header('Access-Control-Allow-Origin', req.headers.origin);
        oneof = true;
    }
    if(req.headers['access-control-request-method']) {
        res.header('Access-Control-Allow-Methods', req.headers['access-control-request-method']);
        oneof = true;
    }
    if(req.headers['access-control-request-headers']) {
        res.header('Access-Control-Allow-Headers', req.headers['access-control-request-headers']);
        oneof = true;
    }
    if(oneof) {
        res.header('Access-Control-Max-Age', 60 * 60 * 24 * 365);
    }

    // intercept OPTIONS method
    if (oneof && req.method == 'OPTIONS') {
        res.send(200);
    }
    else {
        next();
    }
});

// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//statics
app.use(express.static('statics'));

//routes
app.use(require("./routes/routes"));



//run
app.listen(app.get('port'),()=>{
    console.log("server running on ",app.get('port'));
})